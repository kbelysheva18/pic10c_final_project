#include <QMessageBox>
#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent), ui(new Ui::MainWindow),
    database(nullptr), model_teams(nullptr), model_members(nullptr), model_competitions(nullptr), model_scores(nullptr),
    score_calculator(nullptr)
{
    ui->setupUi(this);

    database = new Database();
    bool databaseExists = database->exists();
    database->open();
    if (!databaseExists)
    {
        database->create_tables();
        database->populate_with_data();
    }

    init_model_teams();
    init_model_members();
    init_model_competitions();
    init_model_scores();
}

MainWindow::~MainWindow()
{
    delete ui;
    if (database != nullptr)
        delete database;
    if (model_teams != nullptr)
        delete model_teams;
    if (model_members != nullptr)
        delete model_members;
    if (model_competitions != nullptr)
        delete model_competitions;
    if (model_scores != nullptr)
        delete model_scores;
    if (score_calculator != nullptr)
        delete score_calculator;
}

bool MainWindow::confirm_deletion()
{
    QMessageBox msgBox(this);
    msgBox.setIcon(QMessageBox::Question);
    msgBox.setWindowTitle("Confirmation");
    msgBox.setText("Are you sure you want to delete selected row?");
    msgBox.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
    msgBox.setDefaultButton(QMessageBox::No);
    return msgBox.exec() == QMessageBox::Yes;
}

void MainWindow::show_error(const QString& error)
{
    QMessageBox msgBox(this);
    msgBox.setIcon(QMessageBox::Warning);
    msgBox.setWindowTitle("Error");
    msgBox.setText(error);
    msgBox.setStandardButtons(QMessageBox::Ok);
    msgBox.exec();
}

void MainWindow::show_information(const QString& information)
{
    QMessageBox msgBox(this);
    msgBox.setIcon(QMessageBox::Information);
    msgBox.setWindowTitle("Information");
    msgBox.setText(information);
    msgBox.setStandardButtons(QMessageBox::Ok);
    msgBox.exec();
}

void MainWindow::delete_row(QSqlRelationalTableModel* model, QTableView* table)
{
    int row_index = table->currentIndex().row();
    // Make sure there is selected row and user confirmed deletion.
    if (row_index >= 0 && confirm_deletion()) {
        // Delete row.
        model->removeRow(row_index);
        // Load rows.
        model->select();
        // Select row right after deleted row.
        int row_count = model->rowCount();
        if (row_count > 0) {
            if (row_index >= row_count)
                row_index = row_count - 1;
            table->setCurrentIndex(model->index(row_index, 0));
        }
    }
}

void MainWindow::insert_row(QSqlRelationalTableModel* model)
{
    model->insertRow(model->rowCount(QModelIndex()));
}

void MainWindow::init_model_teams()
{
    // Create data model
    model_teams = new QSqlRelationalTableModel(ui->table_teams);
    model_teams->setEditStrategy(QSqlTableModel::OnRowChange);
    model_teams->setTable("Teams");

    int name_index = model_teams->fieldIndex("Name");

    // Set header captions
    model_teams->setHeaderData(name_index, Qt::Horizontal, "Name");

    // Populate model
    model_teams->select();

    // Set model and hide ID column
    ui->table_teams->setModel(model_teams);
    ui->table_teams->setColumnHidden(model_teams->fieldIndex("Id"), true);
    ui->table_teams->setSelectionMode(QAbstractItemView::SingleSelection);
    ui->table_teams->setSelectionBehavior(QAbstractItemView::SelectRows);

    ui->table_teams->setCurrentIndex(model_teams->index(0, 0));
}

void MainWindow::init_model_members()
{
    // Create data model
    model_members = new QSqlRelationalTableModel(ui->table_members);
    model_members->setEditStrategy(QSqlTableModel::OnRowChange);
    model_members->setTable("Members");

    int team_id_index = model_members->fieldIndex("TeamId");
    int name_index = model_members->fieldIndex("Name");

    model_members->setSort(name_index, Qt::SortOrder::AscendingOrder);

    model_members->setRelation(team_id_index, QSqlRelation("Teams", "Id", "Name"));

    // Set header captions
    model_members->setHeaderData(model_members->fieldIndex("Name"), Qt::Horizontal, "Name");
    model_members->setHeaderData(team_id_index, Qt::Horizontal, "Team");

    model_members->select();

    // Set model and hide ID column
    ui->table_members->setModel(model_members);
    ui->table_members->setItemDelegate(new QSqlRelationalDelegate(ui->table_members));
    ui->table_members->setColumnHidden(model_members->fieldIndex("Id"), true);
    ui->table_members->setSelectionMode(QAbstractItemView::SingleSelection);
    ui->table_members->setSelectionBehavior(QAbstractItemView::SelectRows);

    ui->table_members->setCurrentIndex(model_members->index(0, 0));
}

void MainWindow::init_model_competitions()
{
    // Create data model
    model_competitions = new QSqlRelationalTableModel(ui->table_competitions);
    model_competitions->setEditStrategy(QSqlTableModel::OnRowChange);
    model_competitions->setTable("Competitions");

    int name_index = model_competitions->fieldIndex("Name");

    model_competitions->setSort(name_index, Qt::SortOrder::AscendingOrder);

    // Set header captions
    model_competitions->setHeaderData(model_competitions->fieldIndex("Name"), Qt::Horizontal, "Name");

    model_competitions->select();

    // Set model and hide ID column
    ui->table_competitions->setModel(model_competitions);
    ui->table_competitions->setColumnHidden(model_competitions->fieldIndex("Id"), true);
    ui->table_competitions->setSelectionMode(QAbstractItemView::SingleSelection);
    ui->table_competitions->setSelectionBehavior(QAbstractItemView::SelectRows);

    ui->table_competitions->setCurrentIndex(model_competitions->index(0, 0));
}

void MainWindow::init_model_scores()
{
    // Create data model
    model_scores = new QSqlRelationalTableModel(ui->table_scores);
    model_scores->setEditStrategy(QSqlTableModel::OnRowChange);
    model_scores->setTable("Scores");

    int competition_id_index = model_scores->fieldIndex("CompetitionId");
    int team_id_index = model_scores->fieldIndex("TeamId");

    model_scores->setRelation(competition_id_index, QSqlRelation("Competitions", "Id", "Name"));
    model_scores->setRelation(team_id_index, QSqlRelation("Teams", "Id", "Name"));

    // Set header captions
    model_scores->setHeaderData(competition_id_index, Qt::Horizontal, "Competition");
    model_scores->setHeaderData(model_scores->fieldIndex("RoundName"), Qt::Horizontal, "Round");
    model_scores->setHeaderData(team_id_index, Qt::Horizontal, "Team");
    model_scores->setHeaderData(model_scores->fieldIndex("Side"), Qt::Horizontal, "Side");
    model_scores->setHeaderData(model_scores->fieldIndex("Score"), Qt::Horizontal, "Score");

    model_scores->select();

    // Set model and hide ID column
    ui->table_scores->setModel(model_scores);
    ui->table_scores->setItemDelegate(new QSqlRelationalDelegate(ui->table_scores));
    ui->table_scores->setColumnHidden(model_scores->fieldIndex("Id"), true);
    ui->table_scores->setSelectionMode(QAbstractItemView::SingleSelection);
    ui->table_scores->setSelectionBehavior(QAbstractItemView::SelectRows);

    ui->table_scores->setCurrentIndex(model_scores->index(0, 0));
}

void MainWindow::on_button_add_team_pressed()
{
    insert_row(model_teams);
}

void MainWindow::on_button_remove_team_pressed()
{
    delete_row(model_teams, ui->table_teams);
}

void MainWindow::on_button_add_member_pressed()
{
    insert_row(model_members);
}

void MainWindow::on_button_remove_member_pressed()
{
    delete_row(model_members, ui->table_members);
}

void MainWindow::on_button_add_competition_pressed()
{
    insert_row(model_competitions);
}

void MainWindow::on_button_remove_competition_pressed()
{
    delete_row(model_competitions, ui->table_competitions);
}

void MainWindow::on_button_add_score_pressed()
{
    ui->tabWidget->setCurrentIndex(0);
}

void MainWindow::on_button_remove_score_pressed()
{
    delete_row(model_scores, ui->table_scores);
}

void MainWindow::on_button_calculate_scores_pressed()
{
    // Collect required values.
    int competitionId = ui->combo_competitions->currentData().toInt();
    int teamId = ui->combo_teams->currentData().toInt();
    QString round = ui->combo_rounds->currentText();
    QString side = ui->combo_sides->currentText();

    if (competitionId == 0 || teamId == 0 || round.isEmpty() || side.isEmpty())
    {
        show_error("Please select Competition, Team, Round and Side.");
        return;
    }

    // Collect individual scores.
    score_calculator = new ScoreCalculator(competitionId, teamId, round, side);

    score_calculator->add_prosecution_score("Opening", ui->spinBox_p_opening->value());
    score_calculator->add_prosecution_score("Witness 1", ui->spinBox_p_witness1->value());
    score_calculator->add_prosecution_score("Witness 1 Direct", ui->spinBox_p_direct1->value());
    score_calculator->add_prosecution_score("Witness 2", ui->spinBox_p_witness2->value());
    score_calculator->add_prosecution_score("Witness 2 Direct", ui->spinBox_p_direct2->value());
    score_calculator->add_prosecution_score("Witness 3", ui->spinBox_p_witness3->value());
    score_calculator->add_prosecution_score("Witness 3 Direct", ui->spinBox_p_direct3->value());
    score_calculator->add_prosecution_score("Cross 1", ui->spinBox_p_cross1->value());
    score_calculator->add_prosecution_score("Cross 2", ui->spinBox_p_cross2->value());
    score_calculator->add_prosecution_score("Cross 3", ui->spinBox_p_cross3->value());
    score_calculator->add_prosecution_score("Closing", ui->spinBox_p_closing->value());

    score_calculator->add_defense_score("Opening", ui->spinBox_d_opening->value());
    score_calculator->add_defense_score("Witness 1", ui->spinBox_d_witness1->value());
    score_calculator->add_defense_score("Witness 1 Direct", ui->spinBox_d_direct1->value());
    score_calculator->add_defense_score("Witness 2", ui->spinBox_d_witness2->value());
    score_calculator->add_defense_score("Witness 2 Direct", ui->spinBox_d_direct2->value());
    score_calculator->add_defense_score("Witness 3", ui->spinBox_d_witness3->value());
    score_calculator->add_defense_score("Witness 3 Direct", ui->spinBox_d_direct3->value());
    score_calculator->add_defense_score("Cross 1", ui->spinBox_d_cross1->value());
    score_calculator->add_defense_score("Cross 2", ui->spinBox_d_cross2->value());
    score_calculator->add_defense_score("Cross 3", ui->spinBox_d_cross3->value());
    score_calculator->add_defense_score("Closing", ui->spinBox_d_closing->value());

    // Calculate total score.
    score_calculator->calculate_score();

    // Display final score.
    ui->label_score_value->setText(QString::number(score_calculator->get_score()));

    // Enable "Save" button to allow to save a total score into database.
    ui->button_save_score->setEnabled(true);
}

void MainWindow::on_button_start_new_entry_pressed()
{
    // Enable competition detail controls.
    ui->combo_competitions->setEnabled(true);
    ui->combo_teams->setEnabled(true);
    ui->combo_rounds->setEnabled(true);
    ui->combo_sides->setEnabled(true);
    ui->button_calculate_scores->setEnabled(true);

    // Populate list of competitions.
    auto competitions = database->get_competitions();
    ui->combo_competitions->clear();
    ui->combo_competitions->addItem("");
    for(auto iterator = competitions->begin(); iterator != competitions->end(); iterator++)
        ui->combo_competitions->addItem(iterator->get_name(), QVariant(iterator->get_id()));

    // Populate list of teams.
    auto teams = database->get_teams();
    ui->combo_teams->clear();
    ui->combo_teams->addItem("");
    for(auto iterator = teams->begin(); iterator != teams->end(); iterator++)
        ui->combo_teams->addItem(iterator->get_name(), QVariant(iterator->get_id()));

    // Populate list of rounds.
    ui->combo_rounds->clear();
    ui->combo_rounds->addItem("");
    ui->combo_rounds->addItem("Round 1");
    ui->combo_rounds->addItem("Round 2");
    ui->combo_rounds->addItem("Round 3");
    ui->combo_rounds->addItem("Round 4");

    // Populate list of sides.
    ui->combo_sides->clear();
    ui->combo_sides->addItem("");
    ui->combo_sides->addItem("Prosecution");
    ui->combo_sides->addItem("Defense");

    // Reset individual scores.
    ui->spinBox_p_opening->setValue(0);
    ui->spinBox_p_witness1->setValue(0);
    ui->spinBox_p_witness2->setValue(0);
    ui->spinBox_p_witness3->setValue(0);
    ui->spinBox_p_direct1->setValue(0);
    ui->spinBox_p_direct2->setValue(0);
    ui->spinBox_p_direct3->setValue(0);
    ui->spinBox_p_cross1->setValue(0);
    ui->spinBox_p_cross2->setValue(0);
    ui->spinBox_p_cross3->setValue(0);
    ui->spinBox_p_closing->setValue(0);

    ui->spinBox_d_opening->setValue(0);
    ui->spinBox_d_witness1->setValue(0);
    ui->spinBox_d_witness2->setValue(0);
    ui->spinBox_d_witness3->setValue(0);
    ui->spinBox_d_direct1->setValue(0);
    ui->spinBox_d_direct2->setValue(0);
    ui->spinBox_d_direct3->setValue(0);
    ui->spinBox_d_cross1->setValue(0);
    ui->spinBox_d_cross2->setValue(0);
    ui->spinBox_d_cross3->setValue(0);
    ui->spinBox_d_closing->setValue(0);

    // Reset total score.
    ui->label_score_value->setText("0");
}

void MainWindow::on_button_save_score_pressed()
{
    if (score_calculator != nullptr)
    {
        // Update score in database.
        database->update_score(score_calculator->get_competition_id(), score_calculator->get_team_id(),
                               score_calculator->get_round(), score_calculator->get_side(),
                               score_calculator->get_score());
        // Re-select scores.
        model_scores->select();

        show_information("Score has been successfully saved. Visit the Scores tab to view all scores.");
    }
}
