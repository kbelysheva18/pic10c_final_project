#ifndef COMPETITORSCORE_H
#define COMPETITORSCORE_H

#include <QString>

class CompetitorScore
{
private:
    QString role;
    int score;
    int member_id;
public:
    CompetitorScore()
        : score(0), member_id(0) {}
    CompetitorScore(const QString& _role, int _score, int _member_id = 0)
        : role(_role), score(_score), member_id(_member_id) {}
    const QString& get_role() const;
    int get_score() const;
    int get_member_id() const;
};

#endif // COMPETITORSCORE_H
