#include "competitorscore.h"

const QString& CompetitorScore::get_role() const
{
    return role;
}

int CompetitorScore::get_score() const
{
    return score;
}

int CompetitorScore::get_member_id() const
{
    return member_id;
}
