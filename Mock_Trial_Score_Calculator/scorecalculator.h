#ifndef SCORECALCULATOR_H
#define SCORECALCULATOR_H

#include <QString>
#include <QVector>
#include "competitorscore.h"

class ScoreCalculator
{
private:
    int competition_id;
    int team_id;
    QString round;
    QString side; // side can be Prosecution or Defense
    QVector<CompetitorScore> prosecution_scores;
    QVector<CompetitorScore> defense_scores;
    int total_score;
public:
    ScoreCalculator(int _competition_id, int _team_id, const QString& _round, const QString& _side)
        : competition_id(_competition_id), team_id(_team_id), round(_round), side(_side), total_score(0) {}

    int get_competition_id() const { return competition_id; }
    int get_team_id() const { return team_id; }
    const QString& get_round() const { return round; }
    const QString& get_side() const { return side; }
    int get_score() const { return total_score; }

    void add_prosecution_score(const QString& role, int score);
    void add_defense_score(const QString& role, int score);
    void calculate_score();
};

#endif // SCORECALCULATOR_H
