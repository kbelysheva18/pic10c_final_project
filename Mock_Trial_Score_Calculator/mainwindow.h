#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtSql>
#include <QTableView>
#include "database.h"
#include "scorecalculator.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:

    void on_button_add_team_pressed();

    void on_button_remove_team_pressed();

    void on_button_add_member_pressed();

    void on_button_remove_member_pressed();

    void on_button_add_competition_pressed();

    void on_button_remove_competition_pressed();

    void on_button_calculate_scores_pressed();

    void on_button_start_new_entry_pressed();

    void on_button_add_score_pressed();

    void on_button_remove_score_pressed();

    void on_button_save_score_pressed();

private:
    Ui::MainWindow *ui;
    Database* database;
    QSqlRelationalTableModel* model_teams;
    QSqlRelationalTableModel* model_members;
    QSqlRelationalTableModel* model_competitions;
    QSqlRelationalTableModel* model_scores;
    ScoreCalculator* score_calculator;

    void init_model_teams();
    void init_model_members();
    void init_model_competitions();
    void init_model_scores();

    bool confirm_deletion();
    void show_error(const QString& error);
    void show_information(const QString& information);

    void delete_row(QSqlRelationalTableModel* model, QTableView* table);
    void insert_row(QSqlRelationalTableModel* model);
};

#endif // MAINWINDOW_H
