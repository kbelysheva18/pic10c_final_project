#ifndef DATABASE_H
#define DATABASE_H

#include <memory>

#include <QVector>
#include <QSqlDatabase>
#include <QSqlDriver>
#include <QSqlQuery>
#include <QSqlError>

#include "competition.h"
#include "team.h"

class Database
{
public:
    Database();
    ~Database();

    void open();
    bool exists();
    void create_tables();
    void populate_with_data();
    void update_score(int competition_id, int team_id, const QString& round, const QString& side, int score);

    std::shared_ptr<QVector<Competition>> get_competitions();
    std::shared_ptr<QVector<Team>> get_teams();
private:
    QSqlDatabase db;

    QString get_db_file_path();
    void execute_query(const QString& sql);
    int insert_team(const QString& name);
    int insert_member(const QString& name, int teamId);
    int insert_competition(const QString& name);
    int insert_score(int competition_id, int team_id, const QString& round, const QString& side, int score);
    void delete_score(int competition_id, int team_id, const QString& round, const QString& side);
};

#endif // DATABASE_H
