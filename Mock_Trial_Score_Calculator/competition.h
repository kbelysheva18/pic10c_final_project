#ifndef COMPETITION_H
#define COMPETITION_H

#include <QString>

class Competition
{
private:
    int id;
    QString name;
public:
    Competition() : id(0) {}
    Competition(int _id, const QString _name) : id(_id), name(_name) {}

    int get_id() const { return id; }
    const QString& get_name() const { return name; }
};

#endif // COMPETITION_H
