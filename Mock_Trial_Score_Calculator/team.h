#ifndef TEAM_H
#define TEAM_H

#include <QString>

class Team
{
private:
    int id;
    QString name;
public:
    Team() : id(0) {}
    Team(int _id, const QString _name) : id(_id), name(_name) {}

    int get_id() const { return id; }
    const QString& get_name() const { return name; }
};

#endif // TEAM_H
