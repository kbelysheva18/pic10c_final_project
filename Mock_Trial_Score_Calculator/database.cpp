#include <QDebug>
#include <QStandardPaths>
#include <QDir>
#include <QFile>

#include "database.h"

Database::Database()
{

}

Database::~Database()
{
    if (db.isOpen())
        db.close();
}

QString Database::get_db_file_path()
{
    return QDir::cleanPath(QStandardPaths::standardLocations(QStandardPaths::DocumentsLocation).at(0) + QDir::separator() + "mocktrial.db");
}

void Database::open()
{
    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName(get_db_file_path());
    db.open();
}

bool Database::exists()
{
    return QFile(get_db_file_path()).exists();
}

void Database::execute_query(const QString& sql)
{
    QSqlQuery query(sql);
    //query.exec();
}

int Database::insert_team(const QString& name)
{
    QSqlQuery query("INSERT INTO Teams(Name) VALUES(?)");
    query.addBindValue(name);
    query.exec();
    return query.lastInsertId().toInt();
}

int Database::insert_member(const QString& name, int teamId)
{
    QSqlQuery query("INSERT INTO Members(Name, TeamId) VALUES(?, ?)");
    query.addBindValue(name);
    query.addBindValue(teamId);
    query.exec();
    return query.lastInsertId().toInt();
}

int Database::insert_competition(const QString& name)
{
    QSqlQuery query("INSERT INTO Competitions(Name) VALUES(?)");
    query.addBindValue(name);
    query.exec();
    return query.lastInsertId().toInt();
}

int Database::insert_score(int competition_id, int team_id, const QString& round, const QString& side, int score)
{
    QSqlQuery query("INSERT INTO Scores(CompetitionId, TeamId, RoundName, Side, Score) VALUES(?, ?, ?, ?, ?)");
    query.addBindValue(competition_id);
    query.addBindValue(team_id);
    query.addBindValue(round);
    query.addBindValue(side);
    query.addBindValue(score);
    query.exec();
    return query.lastInsertId().toInt();
}

void Database::delete_score(int competition_id, int team_id, const QString& round, const QString& side)
{
    QSqlQuery query("DELETE FROM Scores WHERE CompetitionId = ? AND TeamId = ? AND RoundName = ? AND Side = ?");
    query.addBindValue(competition_id);
    query.addBindValue(team_id);
    query.addBindValue(round);
    query.addBindValue(side);
    query.exec();
}

void Database::update_score(int competition_id, int team_id, const QString& round, const QString& side, int score)
{
    // Delete old score if it still exists.
    delete_score(competition_id, team_id, round, side);
    // Insert new score.
    insert_score(competition_id, team_id, round, side, score);
}

std::shared_ptr<QVector<Competition>> Database::get_competitions()
{
    auto competitions = std::shared_ptr<QVector<Competition>>(new QVector<Competition>());
    QSqlQuery query("SELECT Id, Name FROM Competitions ORDER BY Name");
    while (query.next())
        competitions->push_back(Competition(query.value(0).toInt(), query.value(1).toString()));
    return competitions;
}

std::shared_ptr<QVector<Team>> Database::get_teams()
{
    auto teams = std::shared_ptr<QVector<Team>>(new QVector<Team>());
    QSqlQuery query("SELECT Id, Name FROM Teams ORDER BY Name");
    while (query.next())
        teams->push_back(Team(query.value(0).toInt(), query.value(1).toString()));
    return teams;
}

void Database::create_tables()
{
    execute_query("CREATE TABLE Teams(Id INTEGER PRIMARY KEY, Name TEXT)");
    execute_query("CREATE TABLE Members(Id INTEGER PRIMARY KEY, Name TEXT, TeamId INTEGER)");
    execute_query("CREATE TABLE Competitions(Id INTEGER PRIMARY KEY, Name TEXT)");
    execute_query("CREATE TABLE Scores(Id INTEGER PRIMARY KEY, " \
                  "CompetitionId INTEGER, TeamId INTEGER, RoundName TEXT, Side TEXT, Score INTEGER)");
}

void Database::populate_with_data()
{
    int team_a_id = insert_team("Team A");
    int team_b_id = insert_team("Team B");
    insert_team("Team C");
    insert_team("Team Blue");
    insert_team("Team Gold");

    insert_member("Kseniya Belysheva", team_a_id);
    insert_member("Isobel Tweedt", team_b_id);
    insert_member("Don Le", team_b_id);
    insert_member("Jonalina Xavanna", team_b_id);
    insert_member("Abigail Phillips", team_a_id);

    int vanderbuilt_2017_id = insert_competition("Vanderbuilt 2017");
    int uclassic_2017_id = insert_competition("UClassic 2017");
    int uclassic_2018_id = insert_competition("UClassic 2018");

    insert_score(uclassic_2017_id, team_a_id, "Round 1", "Prosecution", 5);
    insert_score(uclassic_2017_id, team_a_id, "Round 2", "Defense", 1);

}
