#include <algorithm>
#include "scorecalculator.h"

void ScoreCalculator::add_prosecution_score(const QString& role, int score)
{
    prosecution_scores.append(CompetitorScore(role, score));
}

void ScoreCalculator::add_defense_score(const QString& role, int score)
{
    defense_scores.append(CompetitorScore(role, score));
}

void ScoreCalculator::calculate_score()
{
    int prosecution_score = 0;
    std::for_each(prosecution_scores.begin(), prosecution_scores.end(),
             [&prosecution_score](CompetitorScore& competitor_score){ prosecution_score += competitor_score.get_score(); });

    int defense_score = 0;
    std::for_each(defense_scores.begin(), defense_scores.end(),
             [&defense_score](CompetitorScore& competitor_score){ defense_score += competitor_score.get_score(); });

    total_score = side == "Prosecution" ? prosecution_score - defense_score : defense_score - prosecution_score;
}
