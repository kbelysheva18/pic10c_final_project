# PIC10C_Final_Project


# Topic : College Mock Trial Score calculator: 

In Mock Trial, students compete in the courtroom by putting on a fake trial, with students acting as witnesses and attorneys. 
There are two sides competing against one another : the defense (from one school), and the prosecution (from another school).
During the round, judges give each attorney and witness performance a score between 0 and 10.
Scores for all parts of the trial are added up between both teams at the end of the competition. The side/school that has the most points wins that ballot.


## Goal:

Create a calculator in which the user can easily enter numbers (with either a spinbox or a slider) for each part of the trial. 
The program will add those points and compare it to the opposing team's score, 
showing which team won the ballot, and by how much (composite score). That point difference will appear on the screen once "calculate score" is chosen.
It will also be able to save and store past scores for each competition, as well as view a list of teams, members, and competitions for your school.


# Process:


After figuring out the optimal layout for filling out scores, my next goal was to find a way to have the user first indicate the basic information needed to save the score 
correctly. This included the name of the competition this was for, which round it was, the team competing, and whether they were comepting as prosecution or defense for that round. 

Figured out that by going to the actual user interface where I had the button layout, and left-clicked on a button, I could select "Go to slot", and 
then choose to have something happen when the user changes the number in the spinBox. By doing valueChanged(int) it creates a function within main.cpp 
in which you can write code for what will happen when that spinBox is changed for a different integer. 


I encountered a road block trying to figure out how to correctly update the variables to the values changed in the spinboxes, as the output was not
adding those numbers together accurately.


I encountered an error [debug/moc_mainwindow.cpp] Error 1 and [debug/moc_*cpp] Error 1, which is what happens when class data members are declared in 
signals/slots section. I had mistakenly put all of my value changes based on the spinboxes under the private slot section.

I want this program not only to calculate the composite scores of the mock trial teams, but to also be able to store information for the team (in my
case, for UCLA Mock Trial). 
In addition to the score entries interface, I added tabs for teams, members, and competitions. The teams tab contains the teams that the school using
the program (in my case, UCLA Mock Trial) sends. (For us, there is an A Team, a B Team, etc). The members tab contains all the members in the school's
program. The competitions tab contains all the competitions the school has attended. 
Because throughout the year, teams may change, members may join or quit, and new competitions are attended, I created add and remove buttons for all
those three tabs so that the user could document those changes.


### QSqlRelationalTableModel


In order to create the spreadsheet-like storage of all these lists of teams, members, and competitions, I looked into the module Qt SQL.
Specifically, I researched about the QSqlRelationalTableModel class. The QSqlRelationalTableModel class provides an editable data model 
for a single database table. With this class, I could create a database of members, teams, competitions, and their scores.


### QMessageBox


I decided to also create a message box, like the confirmation message boxes you receive when you are about to do something, and the computer asks 
if you are sure. Specifically, I want that message box to pop up when someone tries to delete anything (team/member/competition) and ask if they 
are sure about the deletion. 
To do this, I used #include <QMessageBox>. QMessageBox is a class that creates a dialog to tell the user something, or to ask the user a question.
I created a QMessageBox object, and included a question mark icon as part of the window that will pop up.
The function setInformativeText is for the message the box will show.
I used the function setStandardButtons to create the button options. However, I also wanted a default button. To achieve that, I used the function
setDefaultButton.


### Adding images


I was able to add an image by creating a label. Originally, I used the class QPixmap to simply set an image into the label, and scale he contents. 
However, I realized that with that method, I would only be able to view that image if I was on my computer. To fix this, I created a resources file
as part of my Qt project, and then simply added it to the picture label in mainwindow.ui. This way, wherever the program was opened and run, the image 
would be able to be viewed because it was now a file directly in the project.


### RAII / SBRM

Resource Acquisition is Initialization, also known as Scope-Based Resource Management.
RAII serves to prevent memory leaks by encapsulating each resource into a class, so when the object goes out of scope, the destructor is automatically
called.
"Scope-Based" refers to the lifetime of the object being bound to the scope of a variable. With Scope-Based Resource Management, even if an exception
is thrown, the objects are still deleted.
Qt implements scope-based resource management because, when a QObject item (specifically, a pointer, in this scenario) goes out of scope, it will be automatically deleted
without me having to manually write delete later on in the code. This is also safer because it also avoids memory leaks even if an exception is thrown.
Qt's object management notifies the parent for the child's destruction (like the way a smart pointer works).


### QVector append() instead of push_back()

In class during discussion, we learned about the difference between using push_back() and emplace_back() to add an item to the end of a container. 
The push_back() function creates a copy of the value it is adding to the end. The emplace_back() function has the same result as the push_back() 
function in that it also adds an item to the end of a container, but it does so by avoiding making a temporary copy. It achieves this by taking in 
the list of arguments as parameters that it can then use to directly construct and object into a container without the temporary copy push_back()
creates.

In the ScoreCalculator class, I create two containers to hold member and opponent scores. However, when I tried to use emplace_back() instead of
push_back() for QVector, I found that QVector actually does not have that function. Instead, it has the function append() with also takes in a list
of arguments and thus avoids the extra temporary copy. Thus, when adding objects of class CompetitorScore to the vectors, I used the append() function.


### Use of Generic Algorithm and Lambda function

In scorecalculator.cpp, I incorporated the concepts we studied in class, by implementing the standard generic algorithm, for_each (instead of using a for loop). Instead of
separately declaring and defining a helper function for for_each, a lambda function is used. 
Specifically, the lambda function adds the total score for the defense side and the prosecution side.


### Disabling and Enabling Spinboxes

The user needs to press "start new entry" on the scores tab, before they can calculate any scores. Until that button is pressed, I disabled the options to choose the other 
options (competition, round, team, side), including disabling the ability to calculate score. Only after the "start new entry" button is clicked, can the user enter in the rest
and actually calculate and save the new score. I did this using the setEnabled function. Until that function is called with the bool parameter "true", those specific functions remain
disabled, meaning those widgets cannot handle keyboard/mouse events, so the button or spinbox cannot be pressed.
Additionally, the "save scores" button is only enabled after the button to calculate scores is clicked. 
Before being enabled, the buttons that are not enabled will be shown as grey. 


### Database

The program holds its data in the SQLite database. There are 4 tables: teams, members, competitions, and scores. Each table has an Id column, which contains a unique number. 
By using this unique Id number, it allows there to be a reference from one record to another without my program having to duplicate the data.
The Teams, Members, and Competition tables are just simple tables that just keep names of the corresponding data.
The Scores table contains scores for competitions, referencing teams and competitions tables (using the previously mentioned Id's).
To change/read the data, I used the SQLanguage database, and the majority of the code that interacts/uses this database can be found in the database class I created.


When the program starts for the first time, it first checks if the database file exists. If that file does not exist, then it creates such a file with some pre-coded data.
After being created, the next time the program reads from the already-created database.
The program has tabs in order to modify the data in Teams, Members, or Competitions, directly through the tabs. 
For Scores, the structure is more complex so there is a separate tab for scores entires. 
When a person hits save, this saves the scores and to view past scores, one can separately look at the scores tab (I added a message box with such a message that will pop up
once the "save score" button is pressed, so the user knows where to look for the newly-added competition score).

